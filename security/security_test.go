package security_test

import (
	"testing"

	"github.com/casbin/casbin"
	"github.com/casbin/casbin/util"
	"gitlab.com/lima1909/casbin-example/security"
)

var enforcer = casbin.NewEnforcer("model.conf", "policy.csv")

func init() {
	util.EnableLog = false
}

func TestBob(t *testing.T) {
	rw := security.New("bob", enforcer)
	err := rw.Read("Yap")
	if err == nil {
		t.Errorf("err expected: %v", err)
	}
	err = rw.Write()
	if err != nil {
		t.Errorf("no err expected: %v", err)
	}
}

func TestAlice(t *testing.T) {
	rw := security.New("alice", enforcer)
	err := rw.Read("Yap")
	if err != nil {
		t.Errorf("no err expected: %v", err)
	}
	err = rw.Write()
	if err != nil {
		t.Errorf("no err expected: %v", err)
	}
}
