package security

import (
	"fmt"

	"github.com/casbin/casbin"
	"gitlab.com/lima1909/casbin-example/service"
)

// SecRWService wrapper for ReadWriter with security
type SecRWService struct {
	rw service.ReadWriter
	e  *casbin.Enforcer
	u  string
}

// New instance of SecRWService as wrapper for RW service
func New(user string, e *casbin.Enforcer) service.ReadWriter {
	return &SecRWService{
		u:  user,
		rw: new(service.RW),
		e:  e,
	}
}

func (s *SecRWService) Read(in string) error {
	if s.e.Enforce(s.u, "ReadWriter", "read") == true {
		s.rw.Read(in)
		return nil
	}
	return fmt.Errorf("read access denied. you need permission read and have: %v", s.e.GetPermissionsForUser(s.u))
}

func (s *SecRWService) Write() error {
	if s.e.Enforce(s.u, "ReadWriter", "write") == true {
		s.rw.Write()
		return nil
	}
	return fmt.Errorf("write access denied")
}
