package service

import "fmt"

// ReadWriter a service for test security
type ReadWriter interface {
	Read(in string) error
	Write() error
}

// RW impl from ReadWriter interface
type RW struct{ v string }

func (rw *RW) Read(in string) error {
	rw.v = in
	return nil
}

func (rw *RW) Write() error {
	fmt.Println(rw.v)
	return nil
}
