package main

import (
	"fmt"

	"github.com/casbin/casbin"
	"github.com/casbin/casbin/util"
	"gitlab.com/lima1909/casbin-example/security"
)

func main() {
	util.EnableLog = true

	e := casbin.NewEnforcer("security/model.conf", "security/policy.csv")

	rw := security.New("bob", e)
	err := rw.Read("Yap")
	if err != nil {
		fmt.Println(err)
	}
	err = rw.Write()
	if err != nil {
		fmt.Println(err)
	}
}
